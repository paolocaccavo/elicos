<!doctype html>
<html lang="it">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Elicos | Drupal specialists</title>
    <meta name="description" content="Elicos fornisce servizi di sviluppo siti internet e CRM in Drupal">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- Fontawesome -->
    <script src="https://kit.fontawesome.com/b5939ad040.js" crossorigin="anonymous" defer></script>

    <!-- Google Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600&display=swap" rel="stylesheet">

    <!-- Stili globali -->
    <link rel="stylesheet" href="./css/style.css">

</head>

<body>

    <?php
        require('./partials/navbar.php');
    ?>

    <!-- Header con Video Background -->
    <div class="header">

        <!-- Video player -->
        <div data-video="omel73mgCq8" class="bg-video js-background-video">
            <div class="video-background">
                <div id="yt-player"></div>
            </div>
        </div>

        <!-- Background image -->
        <div class="bg-video-overlay js-video-overlay" style="background-image: url('https://img.youtube.com/vi/omel73mgCq8/maxresdefault.jpg');"></div>

        <!-- Contenuto above the fold -->
        <header class="header-content">

            <div class="container p-5">

                <div class="row vh-60 align-items-center">

                    <div class="col-12 text-center text-white">

                        <h2 class="display-1 fw-light text-uppercase my-5">We are italian<br class="d-none d-xl-inline"> Drupal specialists</h2>

                        <h3 class="lh-base fw-light mb-5">We promise to our customers: in every new project<br class="d-none d-xl-inline"> we'll challenge ourselves to exploit the<br class="d-none d-xl-inline"> full potential of Drupal.</h3>

                    </div>

                </div>

                <div class="row">
                    <div class="col-12 text-center">
                        <figure class="mb-0">
                            <img src="./img/loghi.png" alt="Loghi" class="img-fluid mx-auto max-700" width="100%">
                        </figure>
                    </div>
                </div>

            </div>

        </header>

    </div>

    <section class="container">

        <div class="row my-5 py-5">

            <div class="col-12 text-center">

                <h2 class="text-success fw-light">Enabling Drupal</h2>

                <p>We work hard to let our customers to leverage the full potential of the<br class="d-none d-xl-inline"> most powerful open-source CMS in the world.</p>

            </div>

        </div>

        <div class="row">

            <div class="col-12 col-lg-4 mb-4">
                <i class="fal fa-lightbulb fa-3x text-success mb-4"></i>
                <h3 class="h1 fw-light mb-4">Development</h3>
                <p>We have been using Drupal as the sole framework for every project in which we're involved in.</p>
                <p>We know every piece of the Drupal code and we have used and customized dozen of popular modules.</p>
                <p>We version our code on Bitbucket or Github, and we provide full or on-demand deployment.</p>
            </div>

            <div class="col-12 col-lg-4 mb-4">
                <i class="fal fa-paper-plane fa-3x text-success mb-4"></i>
                <h3 class="h1 fw-light mb-4">Mentorship</h3>
                <p>Our consultancy service is perfect for whom has to deal with organizational issues related to complex projects.</p>
                <p>We provide suggestions and strategies to whom has to decide how to organize the workflow, as well as to whom has to choose among different modules.</p>
            </div>

            <div class="col-12 col-lg-4 mb-4">
                <i class="fal fa-compass fa-3x text-success mb-4"></i>
                <h3 class="h1 fw-light mb-4">Training</h3>
                <p>There is nothing better then sharing what you've learned.</p>
                <p>This is why we decided to create an Academy for beginners, intermediates and advanced Drupal users. Our training sessions are fully customized on real customers' need.</p>
                <p>No pre-packaged courses, no waste of time.</p>
            </div>

        </div>

    </section>

    <hr class="w-50 mx-auto my-5 text-secondary">

    <section class="container">

        <div class="row mt-5 py-5">

            <div class="col-12 text-center">

                <h2 class="text-success fw-light">We partner with great people</h2>

                <p>We are enthusiastic supporters of the great Drupal community and we are<br class="d-none d-xl-inline"> happy to introduce it to visionaries and innovators.</p>

            </div>

        </div>

        <div class="row py-5">

            <div class="col-12">

                <!-- Navs -->
                <ul class="nav nav-tabs justify-content-around" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link w-100 text-uppercase py-3 px-4 active" id="home-tab" data-bs-toggle="tab" data-bs-target="#developers" type="button" role="tab" aria-controls="home" aria-selected="true" title="Developers"><i class="d-xl-none fal fa-terminal me-2"></i><span class="d-none d-xl-inline">Developers</span></button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link w-100 text-uppercase py-3 px-4" id="profile-tab" data-bs-toggle="tab" data-bs-target="#agencies" type="button" role="tab" aria-controls="profile" aria-selected="false" title="Agencies"><i class="d-xl-none fal fa-users me-2"></i><span class="d-none d-xl-inline">Agencies</span></button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link w-100 text-uppercase py-3 px-4" id="contact-tab" data-bs-toggle="tab" data-bs-target="#ecommerce" type="button" role="tab" aria-controls="contact" aria-selected="false" title="E-commerce managers"><i class="d-xl-none fal fa-store-alt me-2"></i><span class="d-none d-xl-inline">E-commerce managers</span></button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link w-100 text-uppercase py-3 px-4" id="contact-tab" data-bs-toggle="tab" data-bs-target="#startuppers" type="button" role="tab" aria-controls="contact" aria-selected="false" title="Startuppers"><i class="d-xl-none fal fa-rocket me-2"></i><span class="d-none d-xl-inline">Startuppers</span></button>
                    </li>
                </ul>

                <!-- Tabs -->
                <div class="tab-content pt-4 pt-xl-5" id="myTabContent">
                    <div class="tab-pane fade show active" id="developers" role="tabpanel" aria-labelledby="developers-tab">
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <h2 class="mb-4">Perfect for those who need a coding support</h2>
                                <p>We boost the development process of freelances and IT departments by sharing a decade of experience with Drupal.</p>
                                <p>According to our statistics, we have been able to accelerate new deployment of code by shortcutting the choice and configuration of new or existing modules.</p>
                                <p>Our development service relies Kanban metodology for card execution and is based on proper user stories definition. We trace our job within Jira and we version our software on Bitbuket or GitHub.  Every project's weekly sprint is notified to our customers.</p>
                                <p>The interventions we do every day include:</p>
                                <ul class="list-unstyled">
                                    <li class="d-flex align-items-center my-4">
                                        <i class="fal fa-check me-2 text-success"></i>
                                        development, customization, or bug-fixing of modules;
                                    </li>
                                    <li class="d-flex align-items-center my-4">
                                        <i class="fal fa-check me-2 text-success"></i>
                                        training and advice;
                                    </li>
                                    <li class="d-flex align-items-center my-4">
                                        <i class="fal fa-check me-2 text-success"></i>
                                        performing outsourced tasks.
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-lg-6">
                                <figure>
                                    <img src="/img/tab-developers.png" alt="Developers" width="100%" class="img-fluid">
                                </figure>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="agencies" role="tabpanel" aria-labelledby="agencies-tab">
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <h2 class="mb-4">We take care of Drupal tasks, so you can focus on all the rest</h2>
                                <p>Dozens of small and medium sized agencies choose us every year to manage the technical backlog of their projects.</p>
                                <p>Starting with fine tuning of the modules, our developers take care of the full Drupal configuration and deployment of complex projects, in a full-stack mode.</p>
                                <p>We are your ideal partner when:</p>
                                <ul class="list-unstyled">
                                    <li class="d-flex align-items-center my-4">
                                        <i class="fal fa-check me-2 text-success"></i>
                                        you need to start a new project, but your staff is too busy;
                                    </li>
                                    <li class="d-flex align-items-center my-4">
                                        <i class="fal fa-check me-2 text-success"></i>
                                        you need expert knowledge that you currently have not;
                                    </li>
                                    <li class="d-flex align-items-center my-4">
                                        <i class="fal fa-check me-2 text-success"></i>
                                        you want to focus onnly on design or UX and you want to outsource the development.
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-lg-6">
                                <figure>
                                    <img src="/img/tab-agencies.png" alt="Agencies" width="100%" class="img-fluid">
                                </figure>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="ecommerce" role="tabpanel" aria-labelledby="ecommerce-tab">
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <h2 class="mb-4">For those looking for state of the art in online sales</h2>
                                <p>Drupal Commerce is perfect for those who want to combine the power of Magento and the flexibility of Wordpress.</p>
                                <p>We're one of the most skilled Drupal Commerce providers in the Italy, since we have been partnering with prominent brands in several industries: from fashion to automotive.</p>
                                <p>Our service is perfect for those who need expert partners in Drupal Commerce configuration, optimization and integration with esternal services. Furthermore, we help e-commerce managers to build a rich user-experience without compromising performances, SEO and merchandising tools.</p>
                                <p>We boost sales of your e-commerce by:</p>
                                <ul class="list-unstyled">
                                    <li class="d-flex align-items-center my-4">
                                        <i class="fal fa-check me-2 text-success"></i>
                                        developing integration engines, search and automatic import of products;
                                    </li>
                                    <li class="d-flex align-items-center my-4">
                                        <i class="fal fa-check me-2 text-success"></i>
                                        optimizing the architecture and management of taxonomies, product correlations, dis class="my-2"count rules;
                                    </li>
                                    <li>refining checkout process and payment gateway;
        
                                    </li>
                                    <li>integrating Drupal Commerce with established ERPs and touchpoints.
        
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-lg-6">
                                <figure>
                                    <img src="/img/tab-ecommerce.png" alt="Ecommerce" width="100%" class="img-fluid">
                                </figure>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="startuppers" role="tabpanel" aria-labelledby="startuppers-tab">
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <h2 class="mb-4">For those pivoting their new business ideas</h2>
                                <p>You, as a startupper, are looking for a simple, reliable and scalable IT solution for building your MVP.</p>
                                <p>Drupal is the perfect solution for teams at an early stage of their idea development. It is powerful enough to power beta versions for new platforms, and it is quicker then any other framework to be implemented.</p>
                                <p>Furthermore, our team will help you focusing only on high value features, in order to test your ideas and to pivot your business model withouth creating IT path-dependency.</p>
                                <p>With us, you will:</p>
                                <ul class="list-unstyled">
                                    <li class="d-flex align-items-center my-4">
                                        <i class="fal fa-check me-2 text-success"></i>
                                        create MVPs of your platform;
                                    </li>
                                    <li class="d-flex align-items-center my-4">
                                        <i class="fal fa-check me-2 text-success"></i>
                                        implement SAAS business models;
                                    </li>
                                    <li class="d-flex align-items-center my-4">
                                        <i class="fal fa-check me-2 text-success"></i>
                                        test new product features withouth hassle.
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-lg-6">
                                <figure>
                                    <img src="/img/tab-startuppers.png" alt="Startuppers" width="100%" class="img-fluid">
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </section>

    <section class="container-fluid bg-light">

        <div class="container text-center pt-5">

            <div class="row py-5">

                <div class="col-12">

                    <h2 class="text-success fw-light mb-4">We care about the quality of our job</h2>

                    <p class="mb-4">On every new project, our goal is to maximize the customers' satisfaction, by<br class="d-none d-xl-inline"> providing outstanding services.</p>

                </div>

            </div>

            <!-- Counters -->
            <div id="wrapper" class="row pb-5">

                <div class="col-12 col-lg-4 mb-4">
                    <i class="fal fa-code fa-3x text-success mb-3"></i>
                    <p id="count-commits" class="display-4">3701</p>
                    <p>commits, last year</p>
                </div>

                <div class="col-12 col-lg-4 mb-4">
                    <i class="fal fa-users-class fa-3x text-success mb-3"></i>
                    <p id="count-training" class="display-4">750</p>
                    <p>hours of training per year</p>
                </div>

                <div class="col-12 col-lg-4 mb-4">
                    <i class="fal fa-history fa-3x text-success mb-3"></i>
                    <p id="count-work" class="display-4">15320</p>
                    <p>hours worked per year</p>
                </div>

            </div>

        </div>

    </section>

    <section class="container-fluid py-5">

            <div class="row text-center py-5">

                <div class="col-12">

                    <h3 class="h2 fw-light mb-4">See what we can do for you</h3>

                    <a href="#" class="btn btn-lg btn-outline-success text-uppercase font-semibold">Start a project</a>

                </div>

            </div>

    </section>

    <?php
        require('./partials/footer.php');
    ?>

    <!-- Bootstrap & Popper JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Script globali -->
    <script src="./js/script.js"></script>
    
    <!-- Navbar fissa trasparente, che cambia colore allo scroll e da mobile -->
    <script>
    document.addEventListener('DOMContentLoaded', function(){
        let sticky_navbar = document.querySelector('#sticky-navbar');

        if( sticky_navbar && (window.pageYOffset > 30 || screen.width <= 992) )
        {
            sticky_navbar.classList.remove('text-white');
            sticky_navbar.classList.add('bg-light', 'text-dark', 'shadow-sm');
            document.querySelector('#logo').setAttribute('src', '/img/logo-elicos-blue.png');
        }

        document.addEventListener('scroll', function(){
            if( sticky_navbar && (window.pageYOffset > 30 || screen.width <= 992) )
            {
                sticky_navbar.classList.remove('text-white');
                sticky_navbar.classList.add('bg-light', 'text-dark', 'shadow-sm');
                document.querySelector('#logo').setAttribute('src', '/img/logo-elicos-blue.png');
                sticky_navbar.style.transition = ".5s";
            } else {
                sticky_navbar.classList.remove('bg-light', 'text-dark', 'shadow-sm');
                sticky_navbar.classList.add('text-white');
                document.querySelector('#logo').setAttribute('src', '/img/logo-elicos-white.png');
                sticky_navbar.style.transition = ".5s";
            }
        });

    });
    </script>

    <!-- Counters attivati quando compaiono nella pagina -->
    <script>
        function animateValue(obj, start, end, duration) {
            let startTimestamp = null;
            const step = (timestamp) => {
                if (!startTimestamp) startTimestamp = timestamp;
                const progress = Math.min((timestamp - startTimestamp) / duration, 1);
                obj.innerHTML = Math.floor(progress * (end - start) + start);
                if (progress < 1) {
                    window.requestAnimationFrame(step);
                }
            };
            window.requestAnimationFrame(step);
        }

        const commits = document.getElementById("count-commits");
        const training = document.getElementById("count-training");
        const work = document.getElementById("count-work");

        let wrapper = document.getElementById('wrapper');

        let observer = new IntersectionObserver(function(entries) {
            if(entries[0].isIntersecting == true) {
                animateValue(commits, 0, 3701, 1000);
                animateValue(training, 0, 750, 1000);
                animateValue(work, 0, 15320, 1000);
            }
        }, {treshold: [0.5]})

        observer.observe(wrapper)

    </script>

</body>

</html>