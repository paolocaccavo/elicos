<!doctype html>
<html lang="it">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Elicos | Fondazione banco alimentare</title>
    <meta name="description" content="Elicos fornisce servizi di sviluppo siti internet e CRM in Drupal">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- Fontawesome -->
    <script src="https://kit.fontawesome.com/b5939ad040.js" crossorigin="anonymous" defer></script>

    <!-- Google Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600&display=swap" rel="stylesheet">

    <!-- Stili globali -->
    <link rel="stylesheet" href="./css/style.css">

</head>

<body>

    <?php
        require('./partials/navbar.php');
    ?>

    <!-- Contenuto above the fold -->
    <header class="container-fluid bg-fondazione">

        <div class="container py-4">

            <div class="row vh-90 align-items-center">

                <div class="col-12 col-xl-8 text-white">

                    <h2 class="display-2 fw-light mb-3">Fondazione banco alimentare</h2>

                    <h3 class="h4 fw-light mb-5">Drupal e la digital transformation della più grande rete italiana contro la povertà alimentare.</h3>

                </div>

            </div>

            <p class="text-center">
                <a href="#next" class="text-white">
                    <i class="fal fa-chevron-down fa-2x"></i>
                </a>
            </p>

        </div>

    </header>

    <section id="next" class="container pt-5">

        <div class="row">
            <div class="col-12">
                <p><a href="/" class="text-success">Home</a> \ Fondazione banco alimentare</p>
            </div>
        </div>

        <div class="row justify-content-center mb-5 py-5">

            <div class="col-12 col-xl-8">

                <p class="lead fw-normal">La rete Banco Alimentare da 25 anni recupera cibo buono ma non più commercializzabile (proveniente dalla filiera agroalimentare) e lo ridistribuisce a quasi 9.000 strutture caritative di tutto il territorio italiano, contribuendo così a dare un pasto a chi ne ha più bisogno.</p>

            </div>

        </div>

    </section>

    <section class="container-fluid bg-fascia py-5">

        <div class="container">

            <div class="row py-5">

                <div class="col-12 text-center">

                    <h2 class="text-success fw-light mb-4 mb-lg-5">La sfida: costruire un ecosistema digitale con Drupal 7</h2>

                </div>

                <div class="col-12 col-lg-6 my-4">

                    <p class="text-white">La rete Banco Alimentare presenta una struttura geografica piuttosto complessa, composta dalla Fondazione Banco Alimentare (coordinatrice della rete) e da 21 sedi territoriali OBA (Organizzazione Banco Alimentare) capillarmente distribuite in tutta Italia.</p>

                    <p class="text-white">Il progetto, iniziato a ridosso di Expo 2015, in cui Banco Alimentare, date le evidenti affinità tematiche, avrebbe svolto un ruolo di primo piano, era iniziato con l'obiettivo fondamentale di sviluppare un sito responsivo, facilmente consultabile e capace di esprimere l'identità di questa importante non-profit.</p>

                    <p class="text-white">Durante il kickoff meeting, questo brief iniziale si è trasformato in un compito molto più complesso.</p>

                </div>

                <div class="col-12 col-lg-6 my-4">

                    <p class="text-white">Le esigenze di Banco Alimentare erano infatti duplici: da una parte c'era la necessità di avere una unicità visiva, dei percorsi di navigazione e dell'architettura informativa a livello territoriale; dall'altra doveva essere data una elevata autonomia nella gestione dei contenuti a ciascuna sede territoriale, pur mantenendo l'identità comunicativa definita a livello centrale.</p>

                    <p class="text-white">Attraverso un audit approfondito - durato 4 settimane - abbiamo pertanto definito i requisiti funzionali, l'architettura informativa ed il livello di integrazione richiesto fra le diverse componenti di un complesso ecosistema digitale basato su Drupal 7.</p>

                </div>

            </div>

        </div>

    </section>

    <section class="container pt-lg-5">

        <div class="row py-5">

            <div class="col-12 text-center">

                <h4 class="h2 text-success fw-bold mb-5">Le principali attività implementate</h4>
            
                <h2 class="text-success fw-light mb-4 mb-lg-5">Porting da Drupal 5 a Drupal 7</h2>

            </div>

            <div class="col-12 col-lg-6 my-4">

                <p>Il sito di Banco Alimentare lavorava già con la versione 5 del CMS Drupal, e presentava oltre 5.000 contenuti e 40.000 files.</p>

                <p>La principale sfida della migrazione è stata quella di mantenere intatti gli "id" delle varie entità, come nodi, users, termini di tassonomia e files: data la mancata implementazione degli "alias url" sul vecchio Drupal 5, infatti, i vecchi url - già referenziati sul web - dovevano necessariamente riportare alle stesse risorse sul nuovo sito.</p>

            </div>

            <div class="col-12 col-lg-6 my-4">

                <p>Per effettuare il porting dalla vecchia release abbiamo usato i moduli migrate (<a href="https://www.drupal.org/project/migrate" class="text-success" target="_blank">https://www.drupal.org/project/migrate</a>) e migrate d2d (<a href="https://www.drupal.org/project/migrate_d2d" class="text-success" target="_blank">https://www.drupal.org/project/migrate_d2d</a>) mentre è stato necessario realizzare un modulo ad hoc per gestire i files pubblici e privati, oltre che gli attributi "alt" e "title".</p>

                <p>Infine abbiamo mantenuto per alcuni mesi il vecchio sito su un sottodominio (old.bancoalimentare.it), al fine di monitorare ed eventualmente recuperare alcune situazioni nel caso fossero andate perdute durante la migrazione.</p>

            </div>

        </div>

    </section>

    <section class="container-fluid bg-light pt-lg-5">

        <div class="container">

            <div class="row py-5">

                <div class="col-12 text-center">
                
                    <h2 class="text-success fw-light mb-4 mb-lg-5">Organic groups</h2>

                </div>

                <div class="col-12 col-lg-6 my-4">

                    <figure>
                        <img src="./img/organic-groups.jpg" alt="Organic Groups" width="100%" class="img-fluid">
                    </figure>

                </div>

                <div class="col-12 col-lg-6 my-4">

                    <p>Data l'articolazione di Banco Alimentare, abbiamo dovuto implementare una soluzione per la gestione ed il caricamento di contenuti, file, immagini e news da parte di tutti gli editor delle varie OBA, prevenendo al contempo la possibilità di interferire con le pagine di altre OBA o della Fondazione stessa.</p>

                    <p>La situazione è stata gestita ricorrendo al modulo Organic Groups, combinato con Context e con altre funzionalità custom.</p>

                    <p>Queste soluzioni hanno permesso alle singole OBA di avere:</p>

                    <ul class="list-unstyled">
                        <li class="d-flex align-items-baseline my-4">
                            <i class="fal fa-check me-2 text-success"></i>
                            <span>un OBA Admin che gestisce il sito dell'Organizzazione;</span>
                        </li>
                        <li class="d-flex align-items-baseline my-4">
                            <i class="fal fa-check me-2 text-success"></i>
                            <span>un OBA Editor che può pubblicare nel flusso di news della singola OBA e della Fondazione;</span>
                        </li>
                        <li class="d-flex align-items-baseline my-4">
                            <i class="fal fa-check me-2 text-success"></i>
                            <span>un menu di navigazione in parte standard (nome OBA - News - Chi siamo) ed in parte custom in base alle specifiche esigenze della singola organizzazione.</span>
                        </li>
                    </ul>

                    <p>Questa funzionalità avanzata permette di organizzare tutti i contenuti e le pagine di ogni sito senza discostarsi dalle strategie di comunicazione delineate dalla Fondazione, quindi mantenendo un'armonia visiva generale.</p>

                </div>

            </div>

        </div>

    </section>

    <section class="container pt-lg-5">

        <div class="row py-5">

            <div class="col-12 text-center">
            
                <h2 class="text-success fw-light mb-4 mb-lg-5">Single sign on</h2>

            </div>

            <div class="col-12 col-lg-6">

                <figure class="mb-4">
                    <img src="./img/single-sign-on.png" alt="Single sing on" width="100%" class="img-fluid">
                </figure>

                <p>La complicazione principale ha riguardato l'usabilità del complesso sistema di redirect con token di autorizzazioni. Una volta impostato il server OAuth, una buona prassi che abbiamo introdotto per ogni nuovo client è stata di creare dei redirect per le pagine di registrazione, login, gestione del profilo e recupero password: qualsiasi attività (a parte l'assegnazione dei ruoli sul client) relativa alla gestione di un utente avviene quindi sul server.</p>

            </div>

            <div class="col-12 col-lg-6 mb-4">

                <div class="row">
                    <div class="col-12">
                        <figure>
                            <img src="./img/flow-chart.png" alt="Flow chart" width="100%" class="img-fluid">
                        </figure>
                    </div>
                    <div class="col-12 order-lg-first">
                        <p>Sono centinaia gli stakeholders che orbitano attorno alla Fondazione nel ruolo di dipendenti, singole OBA, volontari, uffici stampa e sponsor. Per questo motivo, un altro requisito funzionale molto importante riguardava la possibilità di utilizzare un sistema unico per l'autenticazione e l'accesso alle funzionalità previste per ciascun livello di utenza.</p>

                        <p>Il Single Sign On è stato realizzato con l'implementazione di oauth2_server e oauth2_client, seguendo il modello già usato da Commerce Guys per le loro piattaforme ed ispirato - ad esempio - alla metodologia già implementata con successo da colossi come Google.</p>

                        <p>La nostra soluzione ha portato alla creazione del sito im.bancoalimentare.it, che funge da server OAuth e ospita la gestione di tutte le identità in questione. Il sito funge da database per la gestione degli utenti e dei relativi login sui vari siti del network, per cui effettivamente la gestione del login su un sito come collettaalimentare.it avviene in realtà su im.bancoalimentare.it.</p>
                    </div>
                </div>

            </div>

        </div>

    </section>

    <section class="container-fluid bg-light pt-lg-5">

        <div class="container">

            <div class="row py-5">

                <div class="col-12 text-center">
                
                    <h2 class="text-success fw-light mb-4 mb-lg-5">News publishing flow</h2>

                </div>

                <div class="col-12 col-lg-6 my-4">

                    <figure>
                        <img src="./img/news.png" alt="News publishing flow" width="100%" class="img-fluid">
                    </figure>

                </div>

                <div class="col-12 col-lg-6 my-4">

                    <p>La sezione delle News doveva sia ospitare le notizie provenienti dalla Fondazione che dare spazio a quelle provenienti dalle singole OBA: per permettere una gestione agevole di entrambe le parti è stato implementato un sistema di permessi.</p>

                    <p>All'interno del sistema sono presenti 3 tipi di utenti. Gli editor delle singole organizzazioni territoriali possono pubblicare articoli che compaiono nel news feed del loro organic group (qui un esempio).</p>

                    <p>A questo punto, un network editor con permessi più avanzati può "promuovere" nel news feed generale della rete o in altre landing pages uno o più contenuti provenienti dalle singole organizzazioni (con una logica di pick-up).</p>

                    <p>In questa maniera il news feed è costantemente aggiornato e si raggiunge l'obiettivo di valorizzare la diversità di ciascuna organizzazione territoriale, pur mantenendo una uniformità nella comunicazione.</p>

                </div>

            </div>

        </div>

    </section>

    <section class="container pt-lg-5">

        <div class="row py-5">

            <div class="col-12 text-center">
            
                <h2 class="text-success fw-light mb-4 mb-lg-5">Visual builder per landing pages</h2>

            </div>

            <div class="col-12 col-lg-6 my-4">

                <p>Una novità assoluta rispetto alla precedente versione del sito è stata quella di poter creare delle landing pages senza bisogno di scrivere codice html, utilizzando il nostro potente Elicos Visual Builder ed il modulo Shortcode (<a href="https://www.drupal.org/project/shortcode" target="_blank" class="text-success">https://www.drupal.org/project/shortcode</a>).</p>

                <p>Grazie a questa funzionalità la Fondazione può creare rapidamente campagne che richiedono la disponibilità di landing page attraverso cui generare diversi tipi di conversioni.</p>

                <p>Il Visual Builder mette a disposizione degli editor numerose librerie e template per soddisfare molteplici esigenze creative, attraverso un’interfaccia drag & drop, assicurando al tempo stesso la compatibilità delle pagine create con i più svariati dispositivi. Infatti, gli elementi inseriti tramite il Visual Builder sono basati sul popolare framework responsive Bootstrap.</p>

            </div>

            <div class="col-12 col-lg-6 my-4">

                <p>Tra gli elementi a disposizione troviamo: Bootstrap Rows e Columns per gestire i layout, background con effetti come parallax, slideshow e carousel, link, title, images, embed di blocchi e views, tabs, modal, circles, Google Maps.</p>

                <p>Ogni elemento è configurabile e dispone di un template che può essere adattato a qualsiasi esigenza e tema. Per la Fondazione Banco Alimentare, sono stati anche creati specifici elementi grazie all’implementazione della Shortcode API: infatti, Visual Builder recepisce tutti gli shortcodes di sistema e li compila automaticamente grazie a una comoda form dove inserire i parametri di personalizzazione.</p>

            </div>

        </div>

    </section>

    <section class="container-fluid bg-banco-alimentare py-5">

        <div class="container">

            <div class="row justify-content-center py-5">

                <div class="col-12 col-lg-8 text-center">

                    <!-- Carousel -->
                    <div id="carouselIndicators" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-indicators">
                            <button type="button" data-bs-target="#carouselIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Carousel 1"></button>
                            <button type="button" data-bs-target="#carouselIndicators" data-bs-slide-to="1" aria-label="Carousel 2"></button>
                            <button type="button" data-bs-target="#carouselIndicators" data-bs-slide-to="2" aria-label="Carousel 3"></button>
                        </div>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="./img/carousel-1.png" class="d-block w-100" alt="Carousel 1">
                            </div>
                            <div class="carousel-item">
                                <img src="./img/carousel-2.png" class="d-block w-100" alt="Carousel 2">
                            </div>
                            <div class="carousel-item">
                                <img src="./img/carousel-3.png" class="d-block w-100" alt="Carousel 3">
                            </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselIndicators" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Precedente</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselIndicators" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Successivo</span>
                        </button>
                    </div>

                </div>

            </div>

        </div>

    </section>

    <section class="container py-lg-5">

        <div class="row justify-content-center py-5">

            <div class="col-12 col-lg-8 text-center">
            
                <h2 class="text-success fw-light mb-4 mb-lg-5">Conclusioni</h2>

                <p>Banco Alimentare rappresenta ad oggi uno dei progetti più complessi mai realizzati da Elicos.</p>

                <p>In questo processo di digital transformation, basato su Drupal, siamo riusciti a rendere semplice la grande complessità organizzativa di una rete di migliaia di volontari distribuiti su tutto il territorio nazionale.</p>

            </div>

        </div>

    </section>

    <?php
        require('./partials/footer.php');
    ?>

    <!-- Bootstrap & Popper JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Script globali -->
    <script src="./js/script.js"></script>
    
    <!-- Navbar fissa trasparente, che cambia colore allo scroll e da mobile -->
    <script>
    document.addEventListener('DOMContentLoaded', function(){
        let sticky_navbar = document.querySelector('#sticky-navbar');

        if( sticky_navbar && (window.pageYOffset > 30 || screen.width <= 992) )
        {
            sticky_navbar.classList.remove('text-white');
            sticky_navbar.classList.add('bg-light', 'text-dark', 'shadow-sm');
            document.querySelector('#logo').setAttribute('src', '/img/logo-elicos-blue.png');
        }

        document.addEventListener('scroll', function(){
            if( sticky_navbar && (window.pageYOffset > 30 || screen.width <= 992) )
            {
                sticky_navbar.classList.remove('text-white');
                sticky_navbar.classList.add('bg-light', 'text-dark', 'shadow-sm');
                document.querySelector('#logo').setAttribute('src', '/img/logo-elicos-blue.png');
                sticky_navbar.style.transition = ".5s";
            } else {
                sticky_navbar.classList.remove('bg-light', 'text-dark', 'shadow-sm');
                sticky_navbar.classList.add('text-white');
                document.querySelector('#logo').setAttribute('src', '/img/logo-elicos-white.png');
                sticky_navbar.style.transition = ".5s";
            }
        });

    });
    </script>

</body>

</html>