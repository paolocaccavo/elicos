<!-- Footer -->
<footer class="container-fluid bg-dark py-4">

    <div class="container">

        <div class="row py-5">

            <div class="col-12">

                <h6 class="text-uppercase fw-bold text-secondary">About us</h6>

                <p class="text-secondary mb-5">Our mission is to become the first Italian Drupal Service Provider. Since 2010, we have been committed into developing great softwares that make our customers loving Drupal. We aim at providing the best support on every detail concerning Drupal-related framework - including design, backend configuration, modules' development, third-party integration, theming. Our delivery relies on Play-Kanban®, our customized version of the popular Agile production method. Through this method, we define - step by step - the production backlog, and we keep the development on track.</p>

            </div>
            
            <div class="col-12">

                <ul class="list-unstyled text-secondary mb-5">
                    <li>© 2016 Elicos Srl</li>
                    <li>Via Giovanni XXIII, 45 - 62100 - Macerata (MC) - ITALY</li>
                    <li>P.I. IT 02051920441</li>
                    <li>T +39 0733 20 21 87</li>
                    <li>M <a href="mailto:info@elicos.it" target="_blank">info@elicos.it</a></li>
                </ul>

                <p class="text-secondary">
                    <a href="#" target="_blank">Jobs</a> | <a href="#" target="_blank">Start a project</a><br> <a href="#" target="_blank">Privacy Policy</a> | <a href="#" target="_blank">Cookie Policy</a>
                </p>

            </div>

        </div>

    </div>

</footer>