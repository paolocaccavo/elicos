<!-- Fixed navbar -->
<nav id="sticky-navbar" class="text-white fixed-top">
    <div class="container">
        <div class="d-flex justify-content-between align-items-center py-3">
            <a href="/" class="d-flex align-items-center homelink">
                <figure class="mb-0 me-2">
                    <img id="logo" src="./img/logo-elicos-white.png" alt="Elicos" class="img-fluid" width="50">
                </figure>
                <h1 class="h2">elicos</h1>
            </a>
            <div class="text-end">
                <a href="/project.php" class="btn btn-success rounded-3 py-2 px-3 text-uppercase fw-bold">start a project</a>
            </div>
        </div>
    </div>
</nav>